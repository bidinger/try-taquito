import { Tezos } from '@taquito/taquito';
import { InMemorySigner } from '@taquito/signer';
import { calculator_json } from './calculator';

// identity1 has been generated using the alphanet faucet
// https://tezos.gitlab.io/alphanet/introduction/howtouse.html
// identity2 is just some existing identity on the network
// Babylonnet

const identity1_sk = 'edskS53XfHimRjkrZFgksUpPDnhQaFtphZ5FEwBLBdzMXNVnecCtTNWetC61rU46eMVCpzqop4sqgLXsixEmUkuKkh2FXzGxTn';
const identity1 = 'tz1hCPutTZC9u8zpU5GTokSz1YXtAzEJxkDT';
const identity2 = 'tz1R3vJ5TV8Y5pVj8dicBR23Zv8JArusDkYr';
const server = 'http://node2.sg.tezos.org.sg:8732';

const f = async x => {

    Tezos.setProvider({
        signer: new InMemorySigner(identity1_sk),
        rpc: server,
    });

    try {

        console.log(`retrieve balance of ${identity2}`)
        const bal1 = await Tezos.tz.getBalance(identity2);
        console.log(bal1.c[0])

        const amount = 0.001;
        console.log(`transfer ${amount} from ${identity1} to ${identity2}`)
        const op1 = await Tezos.contract.transfer({ to: identity2, amount: 0.001 });
        console.log(`wait for confirmation for ${op1.hash}`)

        await op1.confirmation();

        const bal2 = await Tezos.tz.getBalance(identity2);
        console.log(bal2.c[0])

        console.log('originate calculator contract')
        const origination = await Tezos.contract.originate({
            code: calculator_json, balance:1, init: '0'
        });
        console.log(`wait for confirmation for ${origination.hash}`)

        const contract = await origination.contract();

        const x = 5;
        const y = 4;
        console.log(`call multiply method of contract (${x}, ${y})`)
        const op2 = await contract.methods.multiply(x, y).send()
        console.log(`wait for confirmation for ${op2.hash}`)
        await op2.confirmation();

        const storage2 = await contract.storage()
        console.log('Storage', storage2.c)

    } catch (e) {
        console.error(e)
    }
}

f()